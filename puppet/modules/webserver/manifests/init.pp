
class webserver {
  file { 'bcnparking.vhost':
    require => Package['apache2'],
    path => "/etc/apache2/sites-available/bcnparking.vhost",
    ensure => present,
    source => 'puppet:///modules/webserver/bcnparking.vhost',
  }
  exec { 'disable_default_site':
      command => '/usr/sbin/a2dissite default',
  }
  exec { 'disable_default-ssl_site':
      command => '/usr/sbin/a2dissite default-ssl',
  }
  exec { 'enable_bcnparking_site':
      command => '/usr/sbin/a2ensite bcnparking.vhost',
  }
  exec { 'enable_mod_rewrite':
      command => '/usr/sbin/a2enmod rewrite',
  }
  File['bcnparking.vhost'] -> Exec['disable_default_site'] -> Exec['disable_default-ssl_site'] -> Exec['enable_bcnparking_site'] -> Exec['enable_mod_rewrite'] ~> Service['apache2']
}