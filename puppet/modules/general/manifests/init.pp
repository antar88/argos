class general {

    # Update software repos
    case $operatingsystem {
        ubuntu: {
            exec { 'repo_update_pre':     # needed since python-software-properties was moved from one repo to another. This could be skipped using a fresher base box (see Vagrantfile).
                command => '/usr/bin/apt-get update',
            }
            package { "python-software-properties":
                ensure => present,
                require => Exec['repo_update_pre'],
            }
            exec { 'add_php5.4_repo':
                command => '/usr/bin/add-apt-repository -y ppa:ondrej/php5',
                unless => '/usr/bin/test -e /usr/bin/php && /usr/bin/php -v | /bin/grep "PHP 5.4" 2>/dev/null',
                require => Package['python-software-properties'],
            }
            exec { 'repo_update':
                command => '/usr/bin/apt-get update',
                require => Exec['add_php5.4_repo'],
            }
        }
        default: {
            notify { "TODO: Implement the update of the repositories for this platform (${operatingsystem})": }
        }
    }

    # Install handy/necessary packages
    $default_packages = [ "git", "vim", "mc", "strace", "sysstat", "curl", "wget", "acl" ]
    package { $default_packages :
        require => Exec['repo_update'],
        ensure => present,
    }

    exec { "set_apache_user_group" :
        command => "/bin/sed -i 's/www-data/vagrant/' /etc/apache2/envvars",
        onlyif  => "/bin/grep -c 'www-data' /etc/apache2/envvars",
        require => Package["apache2"],
        notify  => Service['apache2'],
    }

}