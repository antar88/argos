class debian_repository {
  $sourceName = $::operatingsystem ? {
    ubuntu  => 'precise',
    debian  => 'squeeze',
    default => undef
  }

  $debianName = $::operatingsystem ? {
    ubuntu  => "servergrove-ubuntu-${sourceName}",
    debian  => "servergrove-debian-${sourceName}",
    default => undef
  }

  exec { 'source-update':
    command => 'apt-get update',
    path    => ['/bin', '/usr/bin'],
  }

  # Refreshes the list of packages
  exec { 'apt-get-update':
    command => 'apt-get update',
    path    => ['/bin', '/usr/bin'],
  }
}