class php 
{
    file { "apache2/php.ini":
        path => "/etc/php5/apache2/php.ini",
        ensure => present,
        source => '/vagrant/puppet/files/apache.php.ini',
    }
   package { "php5":
        require => Class['general'],
        before => File['apache2/php.ini'],
        ensure => present,
    }

    file { "cli/php.ini":
        path => "/etc/php5/cli/php.ini",
        ensure => present,
        source => '/vagrant/puppet/files/cli.php.ini',
    }

    package { "php5-cli":
        require => Class['general'],
        before => File['cli/php.ini'],
        ensure => present,
    }

    $php_packages = [ "php-apc", "php-pear", "php5-sqlite", "php5-curl", "php5-intl" ]
    package { $php_packages :
        require => Package['php5'],
        ensure => present,
    }
    
}