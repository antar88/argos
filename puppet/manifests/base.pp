Exec {
    path => "/usr/local/bin:/usr/bin:/usr/sbin:/sbin:/bin",
}

stage { setup: before => Stage[main] }

class { 'repository':
  # Forces the repository to be configured before any other task
  stage => setup
}

include general
include apache
include mysql
include php
include webserver
include composer
