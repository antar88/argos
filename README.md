BCN Parking
=========

Dependencies
------------

Machines need the following installed:

* Git
* Vagrant and VirtualBox
* PHP5.4 (cli) with PDO drivers for mysql

Getting Started
---------------

Clone the repo and:

**The Virtual Machine**

* Add `192.168.50.50 bcnparking` to `/etc/hosts`.
* `cd` to the root dir (where this README file is).
* Run `vagrant up` to run the VM (it will build it since it hasn't been built before).

Thats it!! :) ...now start developing ¬_¬

What you get
------------

**Web interface**

* Accessing from private: http://localhost:4568
* Accessing from public: http://bcnparking:80

**Vagrant VM usefull commands**

* vagrant up
* vagrant halt
* vagrant ssh