<?php

namespace Argos\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SPUSelectorType extends AbstractType
{
    private $spuids;

    public function __construct($spuids = [])
    {
        $this->spuids = $spuids;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('spuid', 'choice', array(
                'choices'   => $this->spuids,
                'multiple'  => false,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Argos\ArgopolisBundle\Entity\SensorProcessUnit',
            'csrf_protection'   => false,
        ));
    }

    public function getName()
    {
        return 'argos_front_spuselectortype';
    }
}
