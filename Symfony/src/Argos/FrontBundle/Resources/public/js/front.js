
var timer = null;

function callmapUpdateAutomatically (id) {
    if(timer) clearInterval(timer);
    mapUpdate(id);
    timer = setInterval(function () { mapUpdate(id); } ,60000);
}

function mapUpdate(id) {
    $('.tooltip').remove();
    $('.dinamicImg').remove();
    $('#loading').show();

    $.get('/api/spu/getMapData/' +  id, null,
        function(sensorData){

            $.each(sensorData, function(index, element) {

                var parking_img = $('#'+element.img).clone();
                var tool_title = '<h6>Adreça: ' + element.address + '</h6><h6>Últim canvi: ' + element.date + '</h6><h6>Estat: ' + element.state + '</h6>';
                tool_title = "<p style='text-align: left'> <strong style='font-size: 12px'>Adreça: </strong><br>" + element.address + "</p>" +
                    "<p style='text-align: left'> <strong style='font-size: 12px'>Últim canvi: </strong><br>" + element.date + "</p>" +
                    "<p style='text-align: left'> <strong style='font-size: 12px'>Estat: </strong><br>" + element.state + "</p>";
                parking_img.attr('id', 'sensorImg'+index);
                parking_img.attr('class', 'dinamicImg');
                parking_img.css('left', element.x+'px');
                parking_img.css('top', element.y+'px');
                parking_img.show();
                parking_img.tooltip({
                    'placement': 'top',
                    'html': true,
                    'title': tool_title
                });
                $('#front-map').append(parking_img);

            });

            $('#loading').hide();
        }, "json");
}

$('#select_map').change(function() {
   var id = $(this).val();
   if (id) {
       $('#front-map').css('background-image', 'url(/upload/maps/' + id + '_map.png)');
       callmapUpdateAutomatically(id)
   }
}).trigger( "change" );

