<?php

namespace Argos\FrontBundle\Controller;

use Argos\AdminBundle\Form\SPUSelectorType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Argos\ArgopolisBundle\Entity\Sensor;
use Argos\Front\Form\SensorType;

class DefaultController extends Controller
{

    private function getAllIds($objects) {
        $properties = [];
        foreach ($objects as $object) {
            if (isset($object)) {
                $desc = '';
                if ($object->getDescription()) {
                    $desc = $object->getDescription();
                }
                $properties[$object->getId()] = ['content' => $object->getId() . ' -> ' .  $desc, 'value' => $object->getId()];

            }
        }

        return $properties;
    }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager()->getRepository('ArgosArgopolisBundle:SensorProcessUnit');
        $selectOptions =  $this->getAllIds($em->findAll());
        return $this->render('ArgosFrontBundle:Default:index.html.twig', ['selectOptions' => $selectOptions]);
    }

}
