<?php
namespace Argos\ArgopolisBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View as View;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Argos\ArgopolisBundle\Entity\Sensor;
use Argos\ArgopolisBundle\Form\SensorType;
use Argos\ArgopolisBundle\Form\SensorUpdateStateType;
use Argos\ArgopolisBundle\Form\SensorSpuAddressType;
use Argos\ArgopolisBundle\Form\SensorRegisterReadingType;
use Argos\ArgopolisBundle\Form\SensorProcessUnitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Argos\ArgopolisBundle\Entity\SensorProcessUnit;

class SensorController extends Controller
{
    // ---------- Private methods ---------
    /**
     * 
     * @param Object $object
     * persist an object to the DDBB
     */
    private function persistObject ($object) {
         $em = $this->getDoctrine()->getManager();
         $em->persist($object);
         $em->flush();
     }
     
     /**
     * 
     * @param Object $object
     * Remove an object to the DDBB
     */
    private function removeObject ($object) {
         $em = $this->getDoctrine()->getManager();
         $em->remove($object);
         $em->flush();
     }
     
     /**
      * Returns the datetime of Europe/Madrid now.
      * @return datetime
      */
     private function getDatetimeNow() {
        $tz = new \DateTimeZone('Europe/Madrid'); 
        $datetime = new \DateTime();
        $datetime->setTimezone($tz);     
        return $datetime;
    }
    
    /**
     * Returns the new state of the sensor depending of the readings
     * @param \Argos\ArgopolisBundle\Entity\Sensor $sensor
     * @param integer $x
     * @param integer $y
     * @return string
     */
    private function getNewState(Sensor $sensor, $x, $y) {
        $emptyX = $sensor->getXEmpty();
        $emptyY = $sensor->getYEmpty();
        
        if($x > 2*$emptyX and $y > 2*$emptyY) return 'full';
        return 'empty';
    }
    
    /**
     * Returns all sensor parameters filled with the request array.
     * @param array $r
     * @param integer $id
     * @return type
     */
    private function getSensorParams ($r, $id = -1) {
        return array(
            "id"    => $id,
            "state" => isset($r['state']) ? $r['state'] : null,
            "data" => isset($r['data']) ? $r['data'] : null,
            "x" => isset($r['x']) ? $r['x'] : null,
            "y" => isset($r['y']) ? $r['y'] : null,
            "sgroup" => isset($r['sgroup']) ? $r['sgroup'] : null,
            "spuid" => isset($r['spuid']) ? $r['spuid'] : null,
            "address" => isset($r['address']) ? $r['address'] : null,
        );
    }
    
    private function patchSensorArrays ($sArrayFill, Sensor $sensor) {
        if (!isset($sArrayFill['id'])) {
            $sArrayFill['id'] = $sensor->getId();
        }
        if (!isset($sArrayFill['state'])) {
            $sArrayFill['state'] = $sensor->getState();
        }
        if (!isset($sArrayFill['data'])) {
            $sArrayFill['data'] = $sensor->getData();
        }
        if (!isset($sArrayFill['x'])) {
            $sArrayFill['x'] = $sensor->getX();
        }
        if (!isset($sArrayFill['y'])) {
            $sArrayFill['y'] = $sensor->getY();
        }
        if (!isset($sArrayFill['sgroup'])) {
            $sArrayFill['sgroup'] = $sensor->getSgroup();
        }
        if (!isset($sArrayFill['spuid'])) {
            $sArrayFill['spuid'] = $sensor->getSpuid();
        }
        if (!isset($sArrayFill['address'])) {
            $sArrayFill['address'] = $sensor->getAddress();
        }
        return $sArrayFill;
    }
    
    /**
     * Return Addresses of the sensors of the SPU
     * @param int $spuid
     * @return array
     * @throws NotFoundHttpException
     */
    private function getAddresses($spuid) 
    {
        $addresses = array();
        $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($spuid);
        if (!$spu instanceof SensorProcessUnit) {
            throw new NotFoundHttpException('Sensor Process Unit not found');
        }
        foreach ($spu->getSensorsIds() as $sensorId) {
            $s = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->find($sensorId);
            if (!$s instanceof Sensor) {
                throw new NotFoundHttpException('Sensor not found');
            }
            $addresses[] = $s->getAddress();
        }
        return $addresses;
    }
    
    /**
     * Returns timeout and all the addresses to read.
     * @param type $spuid
     * @return array
     * @throws NotFoundHttpException
     */
    private function getTimeoutReadings($spuid) {
        $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($spuid);
        if (!$spu instanceof SensorProcessUnit) {
            throw new NotFoundHttpException('Sensor Process Unit not found');
        }      
        return array(
            'timeout'   => $spu->getTimeout(),
            'addresses' => $this->getAddresses($spuid)
        );
    }
    
    // --------- Public methods ----------
    
    /**
     * @ApiDoc(
     *  description="Show all sensors in the system.",
     *  output = "Argos\ArgopolisBundle\Entity\Sensor",
     *  statusCodes={
     *         200="Sensors found",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Getters"
     * )
     * @Rest\View
     */
    public function allAction()
    {
        $sensors = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->findAll();
        return $sensors;
    }

    /**
     * @ApiDoc(
     *  description="Show a concrete sensor.",
     *  output="Argos\ArgopolisBundle\Entity\Sensor",
     *  statusCodes={
     *         200="Sensor with id {id} found",
     *         404="Returned when somehting else is wrong"
     *  },
     *  section = "Getters"
     * )
     * @Rest\View
     */
    public function getAction($id)
    {
        $sensor = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->find($id);       
        if (!$sensor instanceof Sensor) {
            throw new NotFoundHttpException('Sensor not found');
        }      
        return $sensor;
    }
    
    /**
     * @ApiDoc(
     *  description="Show a all sensors with the sensor group {id}.",
     *  output="Argos\ArgopolisBundle\Entity\Sensor",
     *  statusCodes={
     *         200="Sensors with sgroup {id} found",
     *         404="Returned when somehting else is wrong"
     *  },
     *  section = "Getters"
     * )
     * @Rest\View
     */
    public function getByGroupAction($id)
    {
        $sensors = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->findBy(array('sgroup' => $id));           
        if (!isset($sensors)) {
            throw new NotFoundHttpException('Sensor not found');
        }      
        return $sensors;
    }
    
    /**
     * @ApiDoc(
     *  description="Show a sensor from a unique pair spuid and address.",
     *  input="Argos\ArgopolisBundle\Form\SensorSpuAddressType",
     *  output="Argos\ArgopolisBundle\Entity\Sensor",
     *  statusCodes={
     *         200="Sensors with spu and address found",
     *         404="Returned when somehting else is wrong"
     *  },
     *  section = "Getters"
     * )
     * @Rest\View
     */
    public function getBySpuAndAddressAction()
    {
        $reqArr = json_decode($this->getRequest()->getContent(),true);
        $sensors = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->findBy(array('spuid' => $reqArr['spuid'], 'address' => $reqArr['address']));           
        if (!isset($sensors) || count($sensors) == 0) {
            throw new NotFoundHttpException('Sensor not found');
        }      
        return $sensors;
    }
    
    /**
     * @ApiDoc(
     *  description="Show all sensors with this address.",
     *  output="Argos\ArgopolisBundle\Entity\Sensor",
     *  statusCodes={
     *         200="Sensors with address found",
     *         404="Returned when somehting else is wrong"
     *  },
     *  section = "Getters"
     * )
     * @Rest\View
     */
    public function getByAddressAction($address)
    {
        $sensors = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->findBy(array('address' => $address));           
        if (!isset($sensors) || count($sensors) == 0) {
            throw new NotFoundHttpException('Sensor not found');
        }      
        return $sensors;
    }
       
    /**
     * @ApiDoc(
     *  description="Create a new sensor",
     *  input="Argos\ArgopolisBundle\Form\SensorType",
     *  statusCodes={
     *         201="New sensor created successfully",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Creators"
     * )
     * @Rest\View(statusCode=201)
     */
    public function newAction()
    {
        //saving request data
        $reqArr = json_decode($this->getRequest()->getContent(),true);
        $newSensor = new Sensor();
        if($reqArr) {
            //Creating form vars
            $sensorType = new SensorType();
            $form = $this->createForm($sensorType, $newSensor);
            //Submiting the form
            $form->submit($reqArr);
            if (!$form->isValid()) {
                return View::create(print_r($form->getErrorsAsString()), 400);
            }
        }
        
        $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($newSensor->getSpuid());       
        if (!$spu instanceof SensorProcessUnit) {
            throw new NotFoundHttpException('Sensor Process Unit not found');
        }  
        
        //persist the sensor
        $this->persistObject($newSensor);

        //Add sensor to SPU sensors array
        $spu->addSensor($newSensor->getId());
        $this->persistObject($spu);
   
        //Create the response
        $response = new Response();
        $response->setStatusCode(201);
        $response->headers->set('Location',$this->generateUrl('argos_argopolis_sensor_get', array('id' => $newSensor->getId()),true ));
        return $response;
    }
   
    private function processForm($newSensor, $reqArr = null)
    {
        
        //saving request data
        if(!isset($reqArr)) {
            $reqArr = json_decode($this->getRequest()->getContent(),true);
            $reqArr['id'] = $newSensor->getId();
            if(!isset($reqArr['spuid'])) $reqArr['spuid'] = $newSensor->getSpuid();
            if(!isset($reqArr['address'])) $reqArr['address'] = $newSensor->getAddress();
        }
        
        //Creating form vars
        $sensorType = new SensorType();
        $form = $this->createForm($sensorType, $newSensor);
        //Submiting the form
        
        $form->submit($reqArr);
        if ($form->isValid()) {
           
            $em = $this->getDoctrine()->getManager()->getRepository('ArgosArgopolisBundle:Sensor');     
            $sensor = $em->find($reqArr['id']);
            $statusCode = $sensor ? 204 : 201;
            
            //Create the response
            $response = new Response();
            $response->setStatusCode($statusCode);
            
            // set the `Location` header only when creating new resources
            if (201 === $statusCode) {
                $response->headers->set('Location',
                    $this->generateUrl(
                        'argos_argopolis_sensor_get', array('id' => $newSensor->getId()),
                        true // absolute
                    )
                );
            }
            
            //persist the sensor
            
            $this->persistObject($newSensor);

            return $response;
        }
        return View::create(print_r($form->getErrorsAsString()), 400);
    }
    
    /**
     * @ApiDoc(
     *  description="Delete the sensor with id {id}.",
     *  statusCodes={
     *         204="Sensors with id {id} removed from the system",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Delete"
     * )
     * @Rest\View(statusCode=204)
     */
    public function deleteAction($id)
    {
        $sensor = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->find($id);
        if($sensor instanceof Sensor) {
            $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($sensor->getSpuid());
            if($spu instanceof SensorProcessUnit) {
                //remove sensor from SPU
                $spu->removeSensor($id);
                $this->persistObject($spu);
                // remove sensor entity
                $this->removeObject($sensor);
            }
        } 
    }
    
    /**
     * @ApiDoc(
     *  description="Modifies the state of a sensor.",
     *  input = "Argos\ArgopolisBundle\Form\SensorUpdateStateType",
     *  statusCodes={
     *         200="Sensors with id {id} update successfully",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Setters"
     * )
     * @Rest\View(statusCode=200)
     */
    public function changeStateAction() {
        $reqArr = json_decode($this->getRequest()->getContent(),true);
        //creating vars
        $newSensor = new Sensor();
        $sensorType = new SensorType();
        $form = $this->createForm($sensorType, $newSensor);

        //Submiting the form
        $form->submit($reqArr);
        
        if ($form->isValid()) {
            //getting sensor
            $em = $this->getDoctrine()->getManager()->getRepository('ArgosArgopolisBundle:Sensor');
            $sensor = $em->find($reqArr['id']);

            //modifying and persisting the sensor
            $sensor->setState($reqArr['state']);
            $this->persistObject($sensor);
        } else {
            return View::create(print_r($form->getErrorsAsString()), 400);
        }
    }
    
    /**
     * @ApiDoc(
     *  description="Update a sensor with the input data.",
     *  input = "Argos\ArgopolisBundle\Form\SensorType",
     *  statusCodes={
     *         200="Sensors with id {id} update successfully",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Setters"
     * )
     * @Route("/api/sensor/update/{id}")
     * @ParamConverter("sensor", class="ArgosArgopolisBundle:Sensor")
     */
    public function updateAction(Sensor $sensor) {
        return $this->processForm($sensor);
    }
    
    /**
     * @ApiDoc(
     *  description="Update a sensor. The update merge the changes(input + system).",
     *  input = "Argos\ArgopolisBundle\Form\SensorType",
     *  statusCodes={
     *         200="Sensors with id {id} update successfully",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Setters"
     * )
     * @Route("/api/sensor/update/{id}")
     * @ParamConverter("sensor", class="ArgosArgopolisBundle:Sensor")
     */
    public function updatePatchAction(Sensor $sensor) {
        $r = json_decode($this->getRequest()->getContent(),true);
        //Getting sensor structure filled with reqArr
        $reqArr = $this->getSensorParams($r, $sensor->getId());
        //Setting the null params with sensor ones
        $reqArrFilled = $this->patchSensorArrays($reqArr, $sensor);
        //Updating
        return $this->processForm($sensor, $reqArrFilled);
    }

    private function registerIP($spuid, $ip) {
        $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($spuid);
        $spu->setlastReadingIP($ip);
        $this->persistObject($spu);
    }

    /**
     * @ApiDoc(
     *  description="Register a new reading. Input: {spuid,readings:[...{address,x,y}...]} Output: {timeout, addresses:[]}  ",
     *  input = "Argos\ArgopolisBundle\Form\SensorRegisterReadingType",
     *  statusCodes={
     *         200="Sensors with id {id} update successfully",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Setters"
     * )
     * @Rest\View(statusCode=200)
     * 
     */
    public function registerReadingAction () {
        $reqArr = json_decode($this->getRequest()->getContent(),true);
        $spuid = $reqArr['spuid'];
        $readings = $reqArr['readings'];

        foreach ($readings as $reading) {
            $address = $reading['address'];
            $x = $reading['x'];
            $y = $reading['y'];
            
            $sensor = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->findBy(array('spuid' => $spuid, 'address' => $address))[0];
            if (!$sensor instanceof Sensor || !isset($sensor) || count($sensor) === 0 || count($sensor) > 1) {
                return View::create("No sensor found with the addres'" . $address . "' and spuid '" . $spuid . "'", 400);
            }

            //Creating form vars
            $sensorType = new SensorRegisterReadingType();
            $form = $this->createForm($sensorType, new Sensor());
            
            //Submiting the form
            $form->submit($reading);
            if (!$form->isValid()) {
                return View::create(print_r($form->getErrorsAsString()), 400);
            }

            $newState = $this->getNewState($sensor, $x, $y);
            if($sensor->getState() !== 'calibration' and $newState !== $sensor->getState()) {
                //update & persisting
                $sensor->setState($newState);
                $sensor->setDateNow();
            }

            $sensor->setX($x);
            $sensor->setY($y);

            $this->persistObject($sensor);
        }
        $this->registerIP($spuid, $this->getRequest()->getClientIp());
        return $this->getTimeoutReadings($spuid);
    }
}