<?php

namespace Argos\ArgopolisBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View as View;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Argos\ArgopolisBundle\Entity\SensorProcessUnit;
use Argos\ArgopolisBundle\Form\SensorProcessUnitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class SensorProcessUnitController extends Controller {

    /**
     * @ApiDoc(
     *  description="Show all sensor process units in the system.",
     *  output = "Argos\ArgopolisBundle\Entity\SensorProcessUnit",
     *  statusCodes={
     *         200="Sensor Process Units found",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Getters"
     * )
     * @Rest\View
     */
     public function allAction()
    {
        return $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->findAll();
    }
    
    /**
     * @ApiDoc(
     *  description="Show a concrete Sensor Process Unit.",
     *  output="Argos\ArgopolisBundle\Entity\SensorProcessUnit",
     *  statusCodes={
     *         200="Sensor Process Unit with id {id} found",
     *         404="Returned when somehting else is wrong"
     *  },
     *  section = "Getters"
     * )
     * @Rest\View
     */
    public function getAction($id)
    {
        $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($id);       
        if (!$spu instanceof SensorProcessUnit) throw new NotFoundHttpException('Sensor Process Unit not found');      
        return $spu;
    }
    
    /**
     * @ApiDoc(
     *  description="Create a new Sensor Process Unit",
     *  input="Argos\ArgopolisBundle\Entity\SensorProcessUnit",
     *  statusCodes={
     *         201="New Sensor Process Unit created successfully",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Creators"
     * )
     * @Rest\View(statusCode=201)
     */
    public function newAction()
    {
        //saving request data
        $reqArr = json_decode($this->getRequest()->getContent(),true);
        $newSPU = new SensorProcessUnit();
        if($reqArr) {
            //Creating form vars
            $sPUType = new SensorProcessUnitType();
            $form = $this->createForm($sPUType, $newSPU);
            //Submiting the form
            $form->submit($reqArr);
            if(! $form->isValid()) return View::create(print_r($form->getErrorsAsString()), 400);
        }
        
        //persist the sensor Process Unit
        $em = $this->getDoctrine()->getManager();
        $em->persist($newSPU);
        $em->flush();
        
        return $newSPU->getId();
        //Create the response
        $response = new Response();
        $response->setStatusCode(201);
        $response->headers->set('Location',$this->generateUrl('argos_argopolis_spu_get', array('id' => $newSPU->getId()),true ));
        return $response;
    }
    
    /**
     * @ApiDoc(
     *  description="Delete the Sensor Process Unit with id {id}.",
     *  statusCodes={
     *         204="Sensor Process Unit with id {id} removed from the system",
     *         400="Returned when somehting else is wrong"
     *  },
     *  section = "Delete"
     * )
     * @Rest\View(statusCode=204)
     */
    public function deleteAction($id)
    {
        $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($id);
        if($spu instanceof SensorProcessUnit) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($spu);
            $em->flush();
        } 
    }
    
    /**
     * @ApiDoc(
     *  description="Show all sensors for a Process Unit.",
     *  output="Argos\ArgopolisBundle\Entity\SensorProcessUnit",
     *  statusCodes={
     *         200="Sensor Process Unit with id {id} found",
     *         404="Returned when somehting else is wrong"
     *  },
     *  section = "Getters"
     * )
     * @Rest\View
     */
    public function getSensorsAction($id)
    {
        $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($id);     
        if (!$spu instanceof SensorProcessUnit) {
            throw new NotFoundHttpException('Sensor Process Unit not found');
        } 
        $sensors = array();
        foreach ($spu->getSensorsIds() as $sid) {
            if (isset($sid)) {
                $sensors[] = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->find($sid);
            }
        }
        return $sensors;
    }
    
    /**
     * @ApiDoc(
     *  description="Set timezone to SPU.",
     *  output="Argos\ArgopolisBundle\Entity\SensorProcessUnit",
     *  statusCodes={
     *         200="Sensor Process Unit with id {id} modified",
     *         404="Returned when somehting else is wrong"
     *  },
     *  section = "Setters"
     * )
     * @Rest\View
     */
    public function setTimeoutAction ($id)
    {
        $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($id);       
        if (!$spu instanceof SensorProcessUnit) {
            throw new NotFoundHttpException('Sensor Process Unit not found');
        }      
        $spu->setTimeout(json_decode($this->getRequest()->getContent(),true)['timeout']);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($spu);
        $em->flush();
    }

    /**
     * @ApiDoc(
     *  description="Get Map Data for Front End.",
     *  output="Argos\ArgopolisBundle\Entity\SensorProcessUnit",
     *  statusCodes={
     *         200="Data returned successfully",
     *         404="Returned when somehting else is wrong"
     *  },
     *  section = "Getters"
     * )
     * @Rest\View
     */
    public function getMapDataAction ($id) {
        $spu = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($id);
        if (!$spu instanceof SensorProcessUnit) {
            throw new NotFoundHttpException('Sensor Process Unit not found');
        }
        $sensors = [];
        foreach ($spu->getSensorsIds() as $sid) {
            if (isset($sid)) {
                $sensor = $this->getDoctrine()->getRepository('ArgosArgopolisBundle:Sensor')->find($sid);

                $time = '-';
                $time_sensor = $sensor->getDate();

                $tz = new \DateTimeZone('Europe/Madrid');
                $time_now = new \DateTime();
                $time_now->setTimezone($tz);

                if($time_sensor) {
                    $td = $time_sensor->diff($time_now);
                    $time = $td->d . "d $td->h" . "h $td->i" . "m $td->s" . "s";
                }
                $mapData = [
                    'x'         => $sensor->getMapX(),
                    'y'         => $sensor->getMapY(),
                    'img'       => $sensor->getParkingImg(),
                    'address'   => $sensor->getAddress(),
                    'state'     => $sensor->getState(),
                    'date'      => $time
                ];
                $sensors[$sid] = $mapData;
            }
        }
        return $sensors;


    }

}