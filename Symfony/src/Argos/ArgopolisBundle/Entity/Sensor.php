<?php

namespace Argos\ArgopolisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sensor
 */
class Sensor
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $data;

    /**
     * @var integer
     */
    private $x;

    /**
     * @var integer
     */
    private $y;

    /**
     * @var integer
     */
    private $xEmpty;

    /**
     * @var integer
     */
    private $yEmpty;
    
    /**
     * @var integer
     */
    private $xFull;

    /**
     * @var integer
     */
    private $yFull;
    
    /**
     * @var string
     */
    private $sgroup;
    
    /**
     * @var datetime
     */
    private $date;
    
    /**
     * @var integer
     */
    private $spuid;

    /**
     * @var string
     */
    private $address;

    /**
     * @var integer
     */
    private $mapX;

    /**
     * @var integer
     */
    private $mapY;

    /**
     * @var string
     */
    private $parkingType;

    /**
     * Init vars
     */
    public function __construct() {
        $this->state = 'calibration';
        $this->parkingType = 'normal';
        $this->mapX = 0;
        $this->mapY = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Sensor
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return Sensor
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set x
     *
     * @param integer $x
     * @return Sensor
     */
    public function setX($x)
    {
        $this->x = $x;
    
        return $this;
    }

    /**
     * Get x
     *
     * @return integer 
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param integer $y
     * @return Sensor
     */
    public function setY($y)
    {
        $this->y = $y;
    
        return $this;
    }

    /**
     * Get y
     *
     * @return integer 
     */
    public function getY()
    {
        return $this->y;
    }
    
    /**
     * Set xEmpty
     *
     * @param integer $x
     * @return Sensor
     */
    public function setXEmpty($x)
    {
        $this->xEmpty = $x;
    
        return $this;
    }

    /**
     * Get xEmpty
     *
     * @return integer 
     */
    public function getXEmpty()
    {
        return $this->xEmpty;
    }

    /**
     * Set yEmpty
     *
     * @param integer $y
     * @return Sensor
     */
    public function setYEmpty($y)
    {
        $this->yEmpty = $y;
    
        return $this;
    }

    /**
     * Get yEmpty
     *
     * @return integer 
     */
    public function getYEmpty()
    {
        return $this->yEmpty;
    }
    
    /**
     * Set xFull
     *
     * @param integer $x
     * @return Sensor
     */
    public function setXFull($x)
    {
        $this->xFull = $x;
    
        return $this;
    }

    /**
     * Get xFull
     *
     * @return integer 
     */
    public function getXFull()
    {
        return $this->xFull;
    }

    /**
     * Set yFull
     *
     * @param integer $y
     * @return Sensor
     */
    public function setYFull($y)
    {
        $this->yFull = $y;
    
        return $this;
    }

    /**
     * Get yFull
     *
     * @return integer 
     */
    public function getYFull()
    {
        return $this->yFull;
    }


    /**
     * Set sensorGroup
     *
     * @param string $sensorGroup
     * @return Sensor
     */
    public function setSgroup($sensorGroup)
    {
        $this->sgroup = $sensorGroup;
    
        return $this;
    }

    /**
     * Get sensorGroup
     *
     * @return string 
     */
    public function getSgroup()
    {
        return $this->sgroup;
    }
    
    /**
     * Set spuid
     *
     * @param integer $spuid
     * @return Sensor
     */
    public function setSpuid($spuid)
    {
        $this->spuid = $spuid;
    
        return $this;
    }

    /**
     * Get spuid
     *
     * @return integer 
     */
    public function getSpuid()
    {
        return $this->spuid;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Sensor
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * Set date
     *
     * @param datetime $date
     * @return Sensor
     */
    public function setDate ($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Set date Now
     *
     * @return Sensor
     */
    public function setDateNow ()
    {
        $tz = new \DateTimeZone('Europe/Madrid');
        $time_now = new \DateTime();
        $time_now->setTimezone($tz);

        $this->date = $time_now;

        return $this;
    }

    /**
     * Get date
     *
     * @return datetime 
     */
    public function getDate ()
    {
        return $this->date;
    }
    
    /**
     * Get date json
     *
     * @return datetime 
     */
    public function getDateFormated ()
    {
        isset($this->date) ? $res = \date_format( $this->date , "d/m/Y  H:i:s" ) : $res = '';
        return $res;
    }

    /**
     * Set mapX
     *
     * @param integer $mapX
     * @return Sensor
     */
    public function setMapX($mapX)
    {
        $this->mapX = $mapX;

        return $this;
    }

    /**
     * Get mapX
     *
     * @return integer
     */
    public function getMapX()
    {
        return $this->mapX;
    }

    /**
     * Set mapY
     *
     * @param integer $mapY
     * @return Sensor
     */
    public function setMapY($mapY)
    {
        $this->mapY = $mapY;

        return $this;
    }

    /**
     * Get mapY
     *
     * @return integer
     */
    public function getMapY()
    {
        return $this->mapY;
    }

    /**
     * Set parkingType
     *
     * @param string $parkingType
     * @return Sensor
     */
    public function setParkingType($parkingType)
    {
        $this->parkingType = $parkingType;

        return $this;
    }

    /**
     * Get parkingType
     *
     * @return string
     */
    public function getParkingType()
    {
        return $this->parkingType;
    }

    public function getParkingImg() {
        $state = $this->getState();
        if($state == 'excess' or $state == 'busy') return 'parking_' . $state;
        if($this->getParkingType() == 'minus') return 'parking_empty_minus';
        return 'parking_empty';
    }


}