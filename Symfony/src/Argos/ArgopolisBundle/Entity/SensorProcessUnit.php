<?php

namespace Argos\ArgopolisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SensorProcessUnit
 */
class SensorProcessUnit
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $lastReadingIP;

    /**
     * @var integer
     */
    private $timeout;

    /**
     * @var array
     */
    private $sensorsIds;
    
    /**
     * Init vars
     */
    public function __construct() {
        $this->sensorsIds = array();
        $this->description = "";
        $this->timeout = 5000;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SensorProcessUnit
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sensorsIds
     *
     * @param array $sensorsIds
     * @return SensorProcessUnit
     */
    public function setSensorsIds($sensorsIds)
    {
        $this->sensorsIds = $sensorsIds;
        return $this;
    }

    /**
     * Get sensorsIds
     *
     * @return array 
     */
    public function getSensorsIds()
    {
        return $this->sensorsIds;
    }
    
    /**
     * Get timeout
     *
     * @return integer 
     */
    public function getTimeout()
    {
        return $this->timeout;
    }
    
    /**
     * Set timeout
     * @param date $timeout
     * @return SensorProcessUnit 
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
        return $this;
    }
    
    /**
     * Add Sesnor
     */
    public function addSensor($id)
    {
        if (!in_array($id, $this->sensorsIds)) {
            $this->sensorsIds[] = $id;
        }
    }
    
    /**
     * Remove Sesnor
     */
    public function removeSensor($id)
    {
        if(($key = array_search($id, $this->sensorsIds)) !== false) {
            unset($this->sensorsIds[$key]);
        }
    }

    /**
     * Set lastReadingIP
     *
     * @param string $lastReadingIP
     * @return SensorProcessUnit
     */
    public function setlastReadingIP($lastReadingIP)
    {
        $this->lastReadingIP = $lastReadingIP;

        return $this;
    }

    /**
     * Get lastReadingIP
     *
     * @return string
     */
    public function getlastReadingIP()
    {
        return $this->lastReadingIP;
    }
}