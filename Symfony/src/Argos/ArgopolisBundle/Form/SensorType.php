<?php

namespace Argos\ArgopolisBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SensorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('data')
            ->add('state')
            ->add('x')
            ->add('y')
            ->add('sgroup')
            ->add('spuid', 'integer', array('required' => true))
            ->add('address', 'text', array('required' => true))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Argos\ArgopolisBundle\Entity\Sensor',
            'csrf_protection'   => false,
        ));
    }

    public function getName()
    {
        return 'argos_argopolisbundle_sensortype';
    }
}