<?php

namespace Argos\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SensorProcessUnitType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', 'text', array(
                'required' => false, 
                'empty_data'  => ''
              ))
            ->add('timeout')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Argos\ArgopolisBundle\Entity\SensorProcessUnit',
            'csrf_protection'   => false,
        ));
    }

    public function getName()
    {
        return 'argos_adminbundle_sputype';
    }
}











