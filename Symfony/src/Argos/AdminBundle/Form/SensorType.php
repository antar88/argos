<?php

namespace Argos\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SensorType extends AbstractType
{
    private $spuids;

    public function __construct($spuids = [])
    {
        $this->spuids = $spuids;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address')
            ->add('parkingType', 'choice', array(
                'choices'   => array('normal' => 'Normal', 'minus' => 'Disabled'),
                'required'  => true,
            ))
            ->add('mapX', 'integer')
            ->add('mapY', 'integer')
            ->add('spuid', 'choice', array(
                'choices'   => $this->spuids,
                'multiple'  => false,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Argos\ArgopolisBundle\Entity\Sensor',
            'csrf_protection'   => false,
        ));
    }

    public function getName()
    {
        return 'argos_adminbundle_sensortype';
    }
}











