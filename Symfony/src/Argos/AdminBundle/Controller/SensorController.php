<?php

namespace Argos\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Argos\ArgopolisBundle\Entity\Sensor;
use Argos\AdminBundle\Form\SensorType;

/**
 * App controller.
 *
 * @Route("/sensor")
 */
class SensorController extends Controller
{
    private function getSensorTypeWithSelector() {
        $em = $this->getDoctrine()->getManager()->getRepository('ArgosArgopolisBundle:SensorProcessUnit');
        return new SensorType($this->getAllIds($em->findAll()));
    }
    
    private function getAllIds($objects) {
        $properties = [];
        foreach ($objects as $object) {
            if (isset($object)) {
                $desc = '';
                if ($object->getDescription()) {
                    $desc = $object->getDescription();
                }
                $properties[$object->getId()] = $object->getId() . ' -> ' .  $desc;
            }
        }
        return $properties;
    }
    
    
    /**
     * Lists all Sensors entities.
     *
     * @Route("/", name="sensor_list")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ArgosArgopolisBundle:Sensor')->findAll();
        return array('entities' => $entities);
    }
    
    /**
     * Lists all Sensors entities for calibration.
     *
     * @Route("/calibration", name="sensor_calibrate")
     * @Template()
     */
    public function calibrateAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ArgosArgopolisBundle:Sensor')->findBy(['state' => 'calibration']);
        
        //getting latest 3 lectures
        $lectures = [];
        /*foreach ($entites as $entity) {
            $last2lectures = 
            $lectures[$entity->getId()] = 
        }
        */
        return array('entities' => $entities);
    }

    /**
     * Finds and displays an Sensor entity.
     *
     * @Route("/{id}/show", name="sensor_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArgosArgopolisBundle:Sensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sensor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }
    
    /**
     * Put a sensor on calibration mode.
     *
     * @Route("/{id}/calibrate/{state}", name="sensor_calibration_mode")
     */
    public function calibrationModeAction($id, $state)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArgosArgopolisBundle:Sensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sensor entity.');
        }
        if(in_array($state, ['calibration','normal'])) {
            $entity->setState($state);
            $em->persist($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sensor_calibrate'));
    }
    
    /**
     * Put a sensor on calibration mode.
     *
     * @Route("/{id}/calibrate/type/{type}", name="sensor_calibration_value")
     */
    public function calibrationSetValueAction($id, $type)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArgosArgopolisBundle:Sensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sensor entity.');
        }
        if(in_array($type, ['empty','car'])) {
            if($type === 'empty') {
               $entity->setXEmpty($entity->getX()); 
               $entity->setYEmpty($entity->getY());
            }
            else {
               $entity->setXFull($entity->getX()); 
               $entity->setYFull($entity->getY());
            }
            $entity->setX(null); 
            $entity->setY(null);
            $em->persist($entity);
            $em->flush(); 
        }

        return $this->redirect($this->generateUrl('sensor_calibrate'));
    }


    /**
     * Displays a form to create a new Sensor entity.
     *
     * @Route("/new", name="sensor_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Sensor();
        
        $form   = $this->createForm($this->getSensorTypeWithSelector(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Sensor entity.
     *
     * @Route("/create", name="sensor_create")
     * @Method("post")
     * @Template("ArgosAdminBundle:Sensor:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new Sensor();
        $form    = $this->createForm($this->getSensorTypeWithSelector(), $entity);
        
        $form->bind($this->getRequest());
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $emspu = $this->getDoctrine()->getManager();
            $sensor = $emspu->getRepository('ArgosArgopolisBundle:Sensor')->find($entity->getId());
            $spu = $emspu->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($sensor->getSpuid());

            if($spu) {
                $spu->addSensor($entity->getId());
                $emspu->persist($spu);
                $emspu->flush();
            }
            
            

            return $this->redirect($this->generateUrl('sensor_show', ['id' => $entity->getId()]));

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Sensor entity.
     *
     * @Route("/{id}/edit", name="sensor_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArgosArgopolisBundle:Sensor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sensor entity.');
        }

        $editForm = $this->createForm($this->getSensorTypeWithSelector(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Sensor entity.
     *
     * @Route("/{id}/update", name="sensor_update")
     * @Method("post")
     * @Template("ArgosAdminBundle:Sensor:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArgosArgopolisBundle:Sensor')->find($id);
        $prevSpuid = $entity->getSpuid();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sensor entity.');
        }

        $editForm   = $this->createForm($this->getSensorTypeWithSelector(), $entity);
        $deleteForm = $this->createDeleteForm($id);
        $editForm->bind($this->getRequest());

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            if($entity->getSpuid() != $prevSpuid) {
                //Remove sensorform old SPU and added to the new one
                $emspu = $this->getDoctrine()->getManager();
                $spuold = $emspu->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($prevSpuid);
                $spunew = $emspu->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($entity->getSpuid());
                if($spuold) {
                    $spuold->removeSensor($id);
                    $emspu->persist($spuold);
                }
                $spunew->addSensor($id);
                $emspu->persist($spunew);
                
                $emspu->flush();
            }
            return $this->redirect($this->generateUrl('sensor_show', ['id' => $entity->getId()]));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }


    /**
     * Deletes an Sensor entity.
     *
     * @Route("/{id}/delete", name="sensor_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ArgosArgopolisBundle:Sensor')->find($id);
            $spu = $em->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($entity->getSpuid());
            
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sensor entity.');
            }
            
            //remove the sensor on the spu
            if($spu) {
                $spu->removeSensor($id);
            }

            $em->remove($entity);
            $em->persist($spu);
            $em->flush();

        }
        return $this->redirect($this->generateUrl('sensor_list'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

}

