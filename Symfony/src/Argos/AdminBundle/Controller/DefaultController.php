<?php

namespace Argos\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="admin_index")
     * @Template()
     */
    public function indexAction()
    {
        return array(
            'info' => ''
        );
    }

    /**
    * @Route("/cache-clear", name="admin_clear_cache")
    * @Template()
    */
    public function cacheAction()
    {
        exec('php app/console cache:clear --env=prod');
        return $this->render('ArgosAdminBundle:Default:index.html.twig', [
            'info' => 'Cache clear successfully!'
        ]);
    }


}