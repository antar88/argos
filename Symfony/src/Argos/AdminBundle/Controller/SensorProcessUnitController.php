<?php

namespace Argos\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Argos\ArgopolisBundle\Entity\SensorProcessUnit;
use Argos\AdminBundle\Form\SensorProcessUnitType;
use Symfony\Component\HttpFoundation\Response;

/**
 * App controller.
 *
 * @Route("/spu")
 */
class SensorProcessUnitController extends Controller
{    
    /**
     * Lists all spus entities.
     *
     * @Route("/", name="spu_list")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->findAll();
        
        return array('entities' => $entities);
    }

    /**
     * Finds and displays an SPU entity.
     *
     * @Route("/{id}/show", name="spu_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SPU entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new SPU entity.
     *
     * @Route("/new", name="spu_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new SensorProcessUnit();
        $form   = $this->createForm(new SensorProcessUnitType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Experience entity.
     *
     * @Route("/create", name="spu_create")
     * @Method("post")
     * @Template("ArgosAdminBundle:Spu:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new SensorProcessUnit();
        $form    = $this->createForm(new SensorProcessUnitType(), $entity);
        $form->bind($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('spu_show', ['id' => $entity->getId()]));

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing SPU entity.
     *
     * @Route("/{id}/edit", name="spu_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SPU entity.');
        }

        $editForm = $this->createForm(new SensorProcessUnitType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing SPU entity.
     *
     * @Route("/{id}/update", name="spu_update")
     * @Method("post")
     * @Template("ArgosAdminBundle:Spu:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SPU entity.');
        }

        $editForm   = $this->createForm(new SensorProcessUnitType(), $entity);
        $deleteForm = $this->createDeleteForm($id);
        $editForm->bind($this->getRequest());

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('spu_show', ['id' => $entity->getId()]));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }


    /**
     * Deletes an SPU entity.
     *
     * @Route("/{id}/delete", name="spu_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        //TODO Eliminar sensors assosiats
        $form = $this->createDeleteForm($id);
        $form->bind($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SPU entity.');
            }
            
            $sensors = $em->getRepository('ArgosArgopolisBundle:Sensor')->findBy(array('spuid' => $id));
            
            foreach ($sensors as $sensor) {
                $em->remove($sensor);
            }
            
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('spu_list'));
    }
    
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
   /**
     * Deletes an SPU Sensor entity.
     *
     * @Route("/{spuid}/deleteSensor/{sensorId}", name="spu_delete_sensor")
     */
    public function deleteSensorAction($sensorId, $spuid)
    {   
        $em = $this->getDoctrine()->getManager();
        $sensor = $em->getRepository('ArgosArgopolisBundle:Sensor')->find($sensorId);

        if($sensor) {
            $em->remove($sensor);
        }
        $spu = $em->getRepository('ArgosArgopolisBundle:SensorProcessUnit')->find($spuid);
        $spu->removeSensor($sensorId);
        $em->persist($spu);
        $em->flush();
        return $this->redirect($this->generateUrl('spu_show', ['id' => $spuid]));
    }

    /**
     * Displays a form to update Front End Map.
     *
     * @Route("/upload/map/{id}", name="upload_map")
     * @Template()
     */
    public function uploadMapAction($id)
    {
        $form = $this->createFormBuilder()
            ->add('map_image', 'file', [
                'required' => true,
                'label' => ' '
            ])
            ->getForm();

        return array(
            'form'   => $form->createView(),
            'spu_id' => $id,
            'errorMsg' => '',
            'info' =>  ['msg' => 'Please submit only PNG maps.', 'backbutton' => false]
        );
    }


    /**
     * Submited form Front End Map.
     *
     * @Route("/uploaded/map/{id}", name="upload_map_submited")
     * @Method("post")
     */
    public function uploadedMapAction($id)
    {


        $form = $this->createFormBuilder()
            ->add('map_image', 'file', [
                'required' => true,
                'label' => ' '
            ])
            ->getForm();

            $form->bind($this->getRequest());

            if ($form->isValid()) {
                try
                {
                    $dir = $path = $this->get('kernel')->getRootDir() . '/../web/upload/maps/';
                    $filename = $id . "_map.png";

                    $form['map_image']->getData()->move($dir, $filename);



                    $errorMsg = $dir . " " . $filename;
                    return $this->render('ArgosAdminBundle:SensorProcessUnit:uploadMap.html.twig', [
                        'form' => '',
                        'errorMsg' => '',
                        'spu_id' => $id,
                        'info' => ['msg' => 'Map uploaded successfully!', 'backbutton' => true]
                    ]);
                }
                catch( AccessDeniedException $e )
                {
                    throw AccessDeniedBecauseOfInsufficientRolesException( $e.message );
                }


            }
            else {
                return $this->render('ArgosAdminBundle:SensorProcessUnit:uploadMap.html.twig', [
                    'form' => '',
                    'errorMsg' => 'There was an error uploading your map. Please try again.',
                    'spu_id' => $id,
                    'info' => ['msg' => '', 'backbutton' => false]
                ]);
            }

    }

}

