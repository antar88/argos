#!/usr/bin/env bash
add-apt-repository ppa:ondrej/php5
apt-get upgrade -y
apt-get dist-upgrade -y
apt-get update -y

#Apache and default web path configuration
apt-get install -y apache2
rm -rf /var/www
ln -fs /vagrant /var/www

#other utilities
apt-get install -y vim

//PHP 54
#apt-get install python-software-properties
#apt-get install -y php5 libapache2-mod-php5